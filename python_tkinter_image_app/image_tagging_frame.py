"""
    Tkinter-based GUI for editing of image tags and persisting of images with their correct associated tags.
"""
import config, shelve, os, PIL, contextlib
import Tkinter as tk
from PIL import ImageTk, Image

filenames = os.listdir(config.image_source_directory)


# retrieving image objects from persistence
def get_shelved_images():
    temp = []
    with contextlib.closing(shelve.open('images_persistence', 'r')) as shelf:
        for name in filenames:
            temp.append(shelf[name])
    return temp


class ImageTaggingFrame(tk.Frame):
    def __init__(self, main_window):
        tk.Frame.__init__(self, main_window.root)
        # immediately accessible list of the persisted ImageTags objects, to be updated on tag addition
        self.shelved_images = get_shelved_images()
        # index of the currently selected image
        self.image_index = 0

        # setting window size to screen size (fullscreen), may be redundant with 'zoomed' root state
        # self.root.geometry('{}x{}'.format(self.root.winfo_screenwidth(), self.root.winfo_screenheight()))
        self.window_height = main_window.root.winfo_screenheight()
        self.window_width = main_window.root.winfo_screenwidth()

        # creating list of Image objects, stored without names in images[]
        # and list of ImageTk(PhotoImage(Image)) objects, stored without names in tk_images[]
        self.images = []
        self.tkinter_images = []
        resize_height = self.window_height / 2
        resize_width = int(resize_height * 1.7778)

        for name in filenames:
            temp = Image.open(config.image_source_directory + '/' + name)
            temp = temp.resize((resize_width, resize_height), PIL.Image.ANTIALIAS)
            self.images.append(temp)
            self.tkinter_images.append(ImageTk.PhotoImage(temp))

        # frame for the top 1/2 of screen, full width
        self.top_frame = tk.Frame(self, width=self.window_width, height=self.window_height/2, bg="#5162B0")
        self.top_frame.pack(side="top", fill="x")

        # last_image button
        self.last_image_button = tk.Button(self.top_frame, width=10, height=2, text="Previous", command=self.last_image)
        self.last_image_button.configure(background="#EFEDEC")
        self.last_image_button.place(relx=0.15, rely=0.5, anchor='center')

        # Image display Label
        self.panel = tk.Label(self.top_frame, image=self.tkinter_images[0], width=(self.window_height/2)*1.77,
                              height=self.window_height/2)
        self.panel.place(relx=0.5, rely=0.5, anchor='center')

        # next_image button
        self.next_image_button = tk.Button(self.top_frame, width=10, height=2, text="Next", command=self.next_image)
        self.next_image_button.configure(background="#EFEDEC")
        self.next_image_button.place(relx=0.85, rely=0.5, anchor='center')

        # frame for the bottom 1/2 of the screen, full width
        self.bottom_frame = tk.Frame(self, width=self.window_width, height=self.window_height/2)
        self.bottom_frame.configure(bg='#EFEDEC')
        self.bottom_frame.pack(side="bottom")

        # scrollable listbox containing filenames of every image
        self.images_listbox_scrollbar = tk.Scrollbar(self, orient="vertical")
        self.images_listbox = tk.Listbox(self.bottom_frame, yscrollcommand=self.images_listbox_scrollbar.set, width=50)
        self.images_listbox_scrollbar.config(command=self.images_listbox.yview)
        self.images_listbox.bind('<<ListboxSelect>>', self.change_to_selected_image)
        self.images_listbox.place(relx=0.25, anchor="nw")

        # adding filenames to list of selections
        i = 1
        for name in filenames:
            self.images_listbox.insert(i, str(i) + " - " + name)
            i += 1

        # first image has focus initially
        self.images_listbox.selection_set(0)
        self.images_listbox.activate(0)

        # listbox containing tags of currently selected item
        self.tags_listbox_scrollbar = tk.Scrollbar(self, orient="vertical")
        self.tags_listbox = tk.Listbox(self.bottom_frame, yscrollcommand=self.tags_listbox_scrollbar.set)
        self.tags_listbox_scrollbar.config(command=self.tags_listbox.yview)
        self.tags_listbox.place(relx=0.5, anchor="nw")

        # initial population of the tags Listbox with tags of image at index 0
        self.populate_tags_listbox()

        # Entry box to add tags one at a time, with an associated get()-able value
        self.tag_entry_value = tk.StringVar()
        self.tag_entry_box = tk.Entry(self.bottom_frame, textvariable=self.tag_entry_value)
        self.tag_entry_box.place(relx=0.65, rely=0.2, anchor="center")

        # Button to trigger addition of tag from tag_entry_box
        self.tag_entry_button = tk.Button(self.bottom_frame, command=self.add_tag, text="Add Tag")
        self.tag_entry_button.place(relx=0.65, rely=0.28, anchor="center")

        # Button to trigger removal of tag from tags_listbox
        self.tag_removal_button = tk.Button(self.bottom_frame, command=self.remove_tag,
                                            text="Remove Tag")
        self.tag_removal_button.place(relx=0.54, rely=0.48, anchor="center")

        # Button to trigger return to main menu
        self.return_to_main_button = tk.Button(self.bottom_frame, text="Main Menu",
                                               command= main_window.display_main_menu)
        self.return_to_main_button.place(relx=0, rely=1, anchor="sw")

        # keybindings
        main_window.root.bind('<Left>', lambda event: self.left_key(event))
        main_window.root.bind('<Right>', lambda event: self.right_key(event))
        main_window.root.bind('<Return>', lambda event: self.enter_key(event))
        main_window.root.bind('<Delete>', lambda event: self.delete_key(event))

    # change image to a specified index - index selected by clicking from a list of images
    def change_image(self, index):
        self.image_index = index
        self.panel.configure(image=self.tkinter_images[index])
        # clear the tags from the old image
        self.clear_tags_listbox()
        # add in the new image tags
        self.populate_tags_listbox()

    # change image to the next image, and increment image_index accordingly
    def next_image(self):
        # button will clear focus in the listbox
        self.images_listbox.selection_clear(self.image_index)
        if self.image_index < len(self.tkinter_images)-1:
            self.image_index += 1
        # change the displayed image
        self.panel.configure(image=self.tkinter_images[self.image_index])
        # and reset the listbox focus and active to the appropriate image
        self.images_listbox.selection_set(self.image_index)
        self.images_listbox.activate(self.image_index)
        self.images_listbox.see(self.image_index)
        # clear the tags from the old image
        self.clear_tags_listbox()
        # add in the new image tags
        self.populate_tags_listbox()

    # change image to the previous image
    def last_image(self):
        # button will clear focus in the listbox
        self.images_listbox.selection_clear(self.image_index)
        if self.image_index > 0:
            self.image_index -= 1
        # change the displayed image
        self.panel.configure(image=self.tkinter_images[self.image_index])
        # and reset the focus and active to the appropriate image name
        self.images_listbox.selection_set(self.image_index)
        self.images_listbox.activate(self.image_index)
        self.images_listbox.see(self.image_index)
        # clear the tags from the old image
        self.clear_tags_listbox()
        # add in the new image tags
        self.populate_tags_listbox()

    # return the index of the currently selected Listbox item
    def get_selected_image_index(self):
        return_index = self.images_listbox.curselection()[0]
        return return_index

    # changes Label's image to the image selected in Listbox
    def change_to_selected_image(self, event):
        self.change_image(self.get_selected_image_index())

    # replacing persisted ImageTags object with a new ImageTags object with the additional tag
    def add_tag(self):
        if len(self.tag_entry_value.get()) > 0:
            with contextlib.closing(shelve.open('images_persistence', 'w')) as shelf:
                temp_image = shelf[filenames[self.get_selected_image_index()]]
                temp_image.tags.append(self.tag_entry_value.get())
                shelf[filenames[self.get_selected_image_index()]] = temp_image
            # clearing the entry box of the just-entered tag's text
            self.tag_entry_box.delete(0, tk.END)
            # update our immediately accessible list of tags with the new ImageTags object
            self.shelved_images[self.get_selected_image_index()] = temp_image
            # updating tags_listbox to reflect new tag
            self.clear_tags_listbox()
            self.populate_tags_listbox()

    # remove the tag currently selected in tags_listbox from persistence
    def remove_tag(self):
        with contextlib.closing(shelve.open('images_persistence', 'w')) as shelf:
            # retrieving the ImageTags object associated with the currently selected image
            temp_image = shelf[filenames[self.image_index]]
            del temp_image.tags[self.tags_listbox.curselection()[0]]
            # updating persistence with the new ImageTags object with tag removed
            shelf[filenames[self.image_index]] = temp_image
        # update our immediately accessible list of tags with the new ImageTags objects
        self.shelved_images[self.image_index] = temp_image
        # updating tags_listbox to reflect removed tag
        self.clear_tags_listbox()
        self.populate_tags_listbox()

    # populating tags_listbox with tags from shelved_images list
    def populate_tags_listbox(self):
        i=1
        for tag in (self.shelved_images[self.image_index]).tags:
            self.tags_listbox.insert(i, str(tag))
            i += 1

    # removing the tags from tags_listbox
    def clear_tags_listbox(self):
        self.tags_listbox.delete(0, tk.END)

    # keybindings
    def left_key(self, event):
        self.last_image()

    def right_key(self, event):
        self.next_image()

    def enter_key(self, event):
        self.add_tag()

    def delete_key(self, event):
        self.remove_tag()
