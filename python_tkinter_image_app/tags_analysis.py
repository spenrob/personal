import matplotlib, contextlib, shelve, config, os
import Tkinter as tk
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure

matplotlib.use("TkAgg")

user_filenames = os.listdir(config.user_image_source_directory)
image_filenames = os.listdir(config.image_source_directory)

def get_shelved_users():
    users = []
    with contextlib.closing(shelve.open('user_persistence', 'r')) as shelf:
        for name in user_filenames:
            users.append(shelf[name])
    return users

def get_shelved_images():
    images = []
    with contextlib.closing(shelve.open('images_persistence', 'r')) as shelf:
        for name in image_filenames:
            images.append(shelf[name])


# primary container frame to import in main_menu. Will contain subframes with graphs
class TagsAnalysisFrame(tk.Frame):
    def __init__(self, main_window):
        tk.Frame.__init__(self, main_window.root)

        # creating disliked images graph
        self.f = Figure()
        self.f.patch.set_facecolor("#5162B0")
        self.a = self.f.add_subplot(111)

        self.y = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0]
        self.x_strings = ['white', 'black', 'rich', 'old', 'young', 'indian', 'seven', 'eight', 'nine', 'ten', '11']
        self.x = [q for q in range(len(self.x_strings))]

        self.width = 0.5

        self.a.set_xticks(self.x)
        self.a.set_xticklabels(self.x_strings)
        self.a.set_facecolor('#EFEDEC')
        self.a.bar(self.x, self.y, self.width)

        self.canvas = FigureCanvasTkAgg(self.f, self)
        self.canvas.show()
        self.canvas.get_tk_widget().place(relx=0.5, rely=0, relwidth=1, relheight=0.7, anchor="n")

        # scrollable list of users to select from

        self.users_listbox_scrollbar = tk.Scrollbar(self, orient="vertical")
        self.users_listbox = tk.Listbox(self, yscrollcommand=self.users_listbox_scrollbar.set, width=50)
        self.users_listbox_scrollbar.config(command=self.users_listbox.yview)
        self.users_listbox.bind('<<ListboxSelect>>', self.display_user_graphs)
        self.users_listbox.place(relx=0.25, rely=0.5, anchor="nw")


    def display_user_graphs(self):
        pass







class LikedImagesGraphFrame(tk.Frame):
    def __init__(self):
        pass