"""
    Use for a new set of images, or if new images have been added to an existing set
    Syncs the persisted images with the 'images' directory.
    Add support for differently named directories/multiple directories later?
"""
import os, shelve, config, contextlib
from datetime import datetime

class ImageTags:
    def __init__(self, name):
        self.tags = []
        self.filepath = config.image_source_directory + '/' + name

class User:
    def __init__(self, fname, lname):
        self.first_name = fname
        self.last_name = lname
        self.creation_date = datetime.now()
        self.disliked_images = []
        self.liked_images = []
        self.tags = []


def sync():
    # c flag: open database for reading and writing, create db if it doesn't exist
    with contextlib.closing(shelve.open('images_persistence', 'c')) as image_shelf:
        image_keys = image_shelf.keys()
        # 'filename' will be the string name of each image, e.g. 'vest_man.jpg' for an images directory at the same level
        # as this module.
        # Shelving each image in the source directory, if the name of that image is not already an existing key.
        # Newly shelved images will have no associated tags - to be managed in image_tagging module
        for filename in os.listdir(config.image_source_directory):
            if filename not in image_keys:
                image_shelf[filename] = ImageTags(filename)
