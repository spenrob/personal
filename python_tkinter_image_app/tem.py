import matplotlib
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure
import Tkinter as tk


class App:
    def __init__(self):
        self.root = tk.Tk()
        self.root.state('zoomed')
        self.root.update()

        self.frame = testFrame(self)
        self.frame.pack(side='top', fill='both', expand=1)
        self.root.mainloop()


class testFrame(tk.Frame):
    def __init__(self, main_window):
        tk.Frame.__init__(self, main_window.root)
        f = Figure()
        f.patch.set_facecolor("#5162B0")
        a = f.add_subplot(111)

        y = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0]
        x_strings = ['white', 'black', 'rich', 'old', 'young', 'indian', 'seven', 'eight', 'nine', 'ten', '11']
        x = [q for q in range(len(x_strings))]

        width = 0.5

        a.set_xticks(x)
        a.set_xticklabels(x_strings)
        a.set_facecolor('#EFEDEC')
        a.bar(x, y, width)

        canvas = FigureCanvasTkAgg(f, self)
        canvas.show()
        canvas.get_tk_widget().place(relx=0, rely=0, relwidth=1, relheight=1, anchor="nw")


a = App()




