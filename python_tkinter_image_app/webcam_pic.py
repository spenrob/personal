import cv2, os

# Pass in desired filename with a valid extension.
# .jpg preferred
# e.g. take_picture('fname_lname.jpg')

def take_picture(filename):
    cam = cv2.VideoCapture(0)

    cv2.namedWindow("Taking Photo...", cv2.WND_PROP_FULLSCREEN)
    cv2.setWindowProperty("Taking Photo...", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)

    while True:
        ret, frame = cam.read()
        cv2.imshow("Taking Photo...", frame)
        if not ret:
            break
        k = cv2.waitKey(1)

        if k%256 == 32:
            # SPACE pressed, take picture
            cv2.imwrite(os.path.join('user_images', filename), frame)
            break

    cam.release()

    cv2.destroyAllWindows()
