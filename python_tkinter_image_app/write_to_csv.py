import config, shelve, os, contextlib, csv
from display_images import ImageTags, User


def write_to_csv():
    users = []
    images = []
    user_filenames = os.listdir(config.user_image_source_directory)
    image_filenames = os.listdir(config.image_source_directory)

    # unshelving the ImageTags and User objects
    with contextlib.closing(shelve.open('user_persistence', 'r')) as shelf:
        for filename in user_filenames:
            users.append(shelf[filename])
    with contextlib.closing(shelve.open('images_persistence', 'r')) as shelf:
        for filename in image_filenames:
            images.append(shelf[filename])

    # writing user names, image reactions, and tags
    with open('user_tag_values', 'wb') as a:
        writer = csv.writer(a, delimiter=',')
        name = "Order: First_Name Last_Name, Liked_Images, Disliked_Images, Tags"
        writer.writerow([name])
        for index, user in enumerate(users, start=1):
            writer.writerow([index])
            writer.writerow([user.first_name + " " + user.last_name])
            writer.writerow(user.liked_images)
            writer.writerow(user.disliked_images)
            writer.writerow(user.tags)

    # writing image filepaths and tags
    with open('image_tag_values', 'wb') as a:
        writer = csv.writer(a, delimiter=',')
        for image in images:
            row = image.filepath + " " + ','.join(image.tags)
            writer.writerow(row)

write_to_csv()