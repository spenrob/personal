*** May 2017 by Spencer Robinson ***

It should be noted that this project was left unfinished, due to the decision to move from a desktop application to a web application.
(Tag analysis in particular is unfinished.)

Run by executing main_menu.py in an environment with access to a Python 2.7 interpreter.

This project is intended to be provided as an executable file, but full source code is provided in this repository for demonstration
purposes. 

Python-based (Tkinter library) GUI intended to be used as a psychological evaluation tool. Displays a series of images at a constant
time interval. The user is intended to indicate their gut reaction (like or dislike) to each photo within each short interval. 
These reactions are recorded and stored in the local directory by shelving/pickling (conversion to byte stream).

Hotkeys:

- Esc: Exit, stop displaying images.
- Right-arrow: Indicates 'like' of current picture
- Left-arrow: Indicates 'dislike' of current picture
- No keystroke: Indicates lack of dislike

