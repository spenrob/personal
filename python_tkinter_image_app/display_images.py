import cv2, os, random, config, shelve, contextlib, webcam_pic
from datetime import datetime


class ImageTags:
    def __init__(self, source):
        self.tags = []
        self.filepath = config.image_source_directory + '/' + source


class User:
    def __init__(self, fname, lname):
        self.first_name = fname
        self.last_name = lname
        self.creation_date = datetime.now()
        self.disliked_images = []
        self.liked_images = []
        self.tags = []


def test_user(fname, lname):
    current_user = User(fname, lname)

    # list of unnamed instances corresponding to images in source directory
    image_list = [ImageTags(source) for source in os.listdir(config.image_source_directory)]

    # we want to randomly sample from images list so the images are not displayed in a particular order, on the off
    # chance that display order affects reactions to images (e.g. seeing a bunch of beautiful people makes every average
    # person after that seem undesirable by comparison)
    # e.g. converting [0, 1, 2, 3] -> [2, 0, 3, 1] to change order of appearance
    display_order = list(range(0, len(image_list)))
    random.shuffle(display_order)

    cv2.namedWindow('window', cv2.WINDOW_NORMAL)
    cv2.setWindowProperty('window', cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
    cv2.waitKey(1)

    for index in display_order:
        img = cv2.imread(image_list[index].filepath)
        cv2.imshow('window', img)

        # waitKey returns an integer value corresponding to a specific keystroke (e.g. spacebar is 32)
        k = cv2.waitKey(config.time_delay)

        # 27 is the Esc key, allowing users to exit test if they wish.
        #
        if k == 27:
            cv2.destroyWindow('window')
            break
        # If user does not strike a key, waitKey() returns 255. != 255 means user struck any key.
        # If space key (32) was struck, add the filepath of the disliked image to the user's list of
        # disliked images
        elif k == 32:
            current_user.disliked_images.append(image_list[index].filepath)
        # If they didn't press Esc or Space, no reaction, so add that image to the "liked" images
        elif k == 255:
            current_user.liked_images.append(image_list[index].filepath)

        # if the user has reached the last image (i.e. not exited the test early)
        if index == display_order[len(display_order)-1]:
            pic_filename  = current_user.first_name + "_" + current_user.last_name + ".jpg"
            with contextlib.closing(shelve.open('user_persistence', 'c')) as shelf:
                shelf[pic_filename] = current_user

            webcam_pic.take_picture(pic_filename)

    cv2.destroyWindow('window')