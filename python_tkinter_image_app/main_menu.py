import Tkinter as tk
from Tkinter import StringVar
import image_tagging_frame, user_tagging_frame, image_persistence_sync, display_images, tags_analysis, config


class ImageTags:
    def __init__(self, name):
        self.tags = []
        self.filepath = config.image_source_directory + '/' + name

class MainGUI:
    """
    Main menu. Buttons leading to test, image sync, tags analysis, image tagging, and user tagging.
    """
    def __init__(self):
        self.root = tk.Tk()
        self.root.state('zoomed')
        self.root.update()
        self.root.wm_title("Main Menu")

        # initialize window with Main_Menu_Frame
        self.frame = None
        self.display_main_menu()

        self.root.mainloop()

    def display_main_menu(self):
        if self.frame is not None:
            self.frame.pack_forget()
            self.frame.destroy()
        self.frame = MainMenuFrame(self)
        self.frame.pack(side="top", fill="both", expand="1")

    def begin_test(self):
        if self.frame is not None:
            self.frame.pack_forget()
            self.frame.destroy()
        self.frame = BeginTestFrame(self)
        self.frame.pack(side="top", fill="both", expand=1)

    def sync_images(self):
        image_persistence_sync.sync()

    def tag_analysis(self):
        if self.frame is not None:
            self.frame.pack_forget()
            self.frame.destroy()
        self.frame = tags_analysis.TagsAnalysisFrame(self)
        self.frame.pack(side="top", fill="both", expand=1)

    def image_tags(self):
        if self.frame is not None:
            self.frame.pack_forget()
            self.frame.destroy()
        self.frame=image_tagging_frame.ImageTaggingFrame(self)
        self.frame.pack(side="top", fill="both", expand=1)

    def user_tags(self):
        if self.frame is not None:
            self.frame.pack_forget()
            self.frame.destroy()
        self.frame= user_tagging_frame.UserTaggingFrame(self)
        self.frame.pack(side="top", fill="both", expand=1)


class MainMenuFrame(tk.Frame):
    def __init__(self, main_window):
        tk.Frame.__init__(self, main_window.root)

        self.button_begin_test = tk.Button(self, text="Test", command=main_window.begin_test)
        self.button_sync_images = tk.Button(self, text="Sync Images", command=main_window.sync_images)
        self.button_tag_analysis = tk.Button(self, text="Tag Analysis", command=main_window.tag_analysis)
        self.button_image_tags = tk.Button(self, text="Image Tags", command=main_window.image_tags)
        self.button_user_tags = tk.Button(self, text="User Tags", command=main_window.user_tags)

        self.button_begin_test.place(relx=0.5, rely=0.3, relwidth=0.05, anchor="center")
        self.button_sync_images.place(relx=0.5, rely=0.37, relwidth=0.05, anchor="center")
        self.button_tag_analysis.place(relx=0.5, rely=0.44, relwidth=0.05, anchor="center")
        self.button_image_tags.place(relx=0.5, rely=0.51, relwidth=0.05, anchor="center")
        self.button_user_tags.place(relx=0.5, rely=0.58, relwidth=0.05, anchor="center")


class BeginTestFrame(tk.Frame):
    def __init__(self, main_window):
        tk.Frame.__init__(self, main_window.root)

        # entry box for user first name
        self.first_name_label = tk.Label(self, text="First Name: ")
        self.first_name_value = tk.StringVar()
        self.first_name_entry = tk.Entry(self, textvariable=self.first_name_value)
        self.first_name_entry.place(relx=0.4, rely=0.2, anchor="center")
        self.first_name_label.place(relx=0.33, rely=0.2, anchor="center")

        # entry box for user last name
        self.last_name_label = tk.Label(text="Last Name: ")
        self.last_name_value = tk.StringVar()
        self.last_name_entry = tk.Entry(self, textvariable=self.last_name_value)
        self.last_name_entry.place(relx=0.6, rely=0.2, anchor="center")
        self.last_name_label.place(relx=0.53, rely=0.2, anchor="center")

        self.button_begin_test = tk.Button(self, text="Begin Test", command= self.begin_test)
        self.button_begin_test.place(relx=0.5, rely=0.3, anchor="center")

    def begin_test(self):
        display_images.test_user(self.first_name_value.get(), self.last_name_value.get())

if __name__ == "__main__":
    gui = MainGUI()