# Delay (ms) between displaying of images. i.e. How much time the user has to react to an image
time_delay = 750

# Directory of images
image_source_directory = 'images'
user_image_source_directory= 'user_images'